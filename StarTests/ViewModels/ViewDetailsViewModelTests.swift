//
//  ViewDetailsViewModelTests.swift
//  StarTests
//
//  Created by Jacob Chan on 11/26/20.
//  Copyright © 2020 Appetiser. All rights reserved.
//

import XCTest

@testable import Star

final class ViewDetailsViewModelTests: XCTestCase {
    func testInit() {
        let star = Star()
        let viewModel = ViewDetailsViewModel(star: star)
        XCTAssertNil(viewModel.star.artistName)
    }
}
