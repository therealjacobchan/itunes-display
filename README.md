# STAR
> Star is a project for the coding challenge for Appetiser apps.

The app basically displays a list of movies from a iTunes search, and tags them if they have been seen by the user or not..

## Features

- Persistence using UserDefaults
- MVVM Architecture
- Movie List
- Movie Details

## Requirements

- iOS 9.0+
- Xcode 12.2

# Installation

Using the Terminal, Go to the project folder

```sh
$ cd $HOME/Desktop
```

### Presentation

Checkout release branch SSH
```sh
$ git clone git@bitbucket.org:therealjacobchan/star.git
```

Checkout release branch HTTP
```sh
$ git clone https://therealjacobchan@bitbucket.org/therealjacobchan/star.git
```

Pull the most updated changes
```sh
$ git pull  
```


### Cocoapods setup

Install all the libraries in cocoapods. Requires Cocoapods 1.8.4
```sh
$ pod install
```

### Xcode Setup

In Xcode, Build > Clean and then Compile!

NOTE: This will require Xcode 12 and iOS 14.1.

## Rationale for Persistence

The persistence feature of the application is found in the UserDefaultsCredentials.swift file. The reason for this is to enable the app to have an abstraction layer for the persistence of the app. This is meant to decouple the actual feature of the app that may otherwise use persistence, like when the user taps an item.

## Rationale for MVVM Architecture

The reason why I chose the MVVM architecture for this project is that it decouples a lot of the processes that may otherwise be done in a tightly coupled fashion. That means that if I did not use MVVM, all logic will be bound to the view of the app, which may further cause confusion later on.

Another reason as to why I used MVVM is testability. I haven't really explored on this yet, but I believe that a good application in general should implement guidelines for unit and UI testing. 

However, one disadvantage of the MVVM architecture is that it tends to expand file size too much. At the same time, it presents a lot of abstractions, in which if scaled to a larger project, may cause several problems later on.

## Meta

Jacob Chan – jacobchan_19@yahoo.com
