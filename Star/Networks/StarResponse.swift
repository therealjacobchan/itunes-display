//
//  StarResponse.swift
//  Star
//
//  Created by Jacob Chan on 11/26/20.
//  Copyright © 2020 Appetiser. All rights reserved.
//

import Foundation

public class StarResponse<T: Decodable>: Decodable {
    /// Aray of results, would be empty if response is 204
    public let resultCount: Int?
    public let results: [Star]
}
