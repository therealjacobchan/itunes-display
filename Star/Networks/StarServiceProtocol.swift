//
//  StarServiceProtocol.swift
//  Star
//
//  Created by Jacob Chan on 11/26/20.
//  Copyright © 2020 Appetiser. All rights reserved.
//

import Foundation

public protocol StarServiceProtocol {
    @discardableResult
    func getResults(completion: @escaping (APIResponse<[Star]?>) -> Void) -> CancelableRequest
}
