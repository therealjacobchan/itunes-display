//
//  CancelableRequest.swift
//  Star
//
//  Created by Jacob Chan on 11/26/20.
//  Copyright © 2020 Appetiser. All rights reserved.
//

import Alamofire

public protocol CancelableRequest {
    @discardableResult
    func cancel() -> Self
}

extension DataRequest: CancelableRequest { }
