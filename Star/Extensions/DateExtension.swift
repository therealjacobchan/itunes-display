//
//  DateExtension.swift
//  Star
//
//  Created by Jacob Chan on 11/27/20.
//  Copyright © 2020 Clear. All rights reserved.
//

import Foundation

extension Date {
	func timeAgo() -> String {
	let secondsAgo = Int(Date().timeIntervalSince(self))
	let formatter = DateFormatter()

	if secondsAgo < 86400 {
		formatter.dateFormat = "HH:mm"
	} else if secondsAgo < 31556926{
		formatter.dateFormat = "dd MMM"
	} else {
		formatter.dateFormat = "MM/dd/yyyy"
	}
	return formatter.string(from: self)
}
}
