//
//  ViewController.swift
//  Star
//
//  Created by Jacob Chan on 11/26/20.
//  Copyright © 2020 Appetiser. All rights reserved.
//

import UIKit

public class ViewController: UIViewController {

    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.isAccessibilityElement = true
        tableView.accessibilityIdentifier = "tableView"
        tableView.delegate = self
        tableView.dataSource = self
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 80
        tableView.registerCell(TableViewCell.self)
        tableView.separatorInset = .zero
        tableView.backgroundColor = .systemBackground
        tableView.tableFooterView = UIView()
        tableView.allowsSelection = true
        return tableView
    }()

    private lazy var activityIndicatorView: UIActivityIndicatorView = {
        let avatarActivityIndicator = UIActivityIndicatorView()
        avatarActivityIndicator.color = UIColor.black
        avatarActivityIndicator.translatesAutoresizingMaskIntoConstraints = false
        avatarActivityIndicator.hidesWhenStopped = true
        return avatarActivityIndicator
    }()

    private lazy var userDefaults: UserDefaultCredentials = {
        let userDefaults = UserDefaultCredentials()
        return userDefaults
    }()

    private let viewModel: ViewModel

    public init(viewModel: ViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    public override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Star"
        self.navigationController?.navigationBar.accessibilityIdentifier = "mainNavigationBar"

        self.extendedLayoutIncludesOpaqueBars = true
        self.edgesForExtendedLayout = UIRectEdge.top
        view.addSubview(tableView)
        view.addSubview(activityIndicatorView)
        NSLayoutConstraint.activate([
            activityIndicatorView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            activityIndicatorView.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            tableView.topAnchor.constraint(equalTo: view.topAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            tableView.rightAnchor.constraint(equalTo: view.rightAnchor),
            tableView.leftAnchor.constraint(equalTo: view.leftAnchor)
        ])
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "Back", style: .plain, target: nil, action: nil)

        self.activityIndicatorView.startAnimating()
        viewModel.successfulFetch = { [weak self] (result, response) in
            self?.activityIndicatorView.stopAnimating()
            if result {
                self?.tableView.reloadData()
            }
        }

        do {
            if let savedItem = try self.userDefaults.getObject(forKey: "selected_item", castTo: Star.self) {
				savedItem.lastViewed = Date()
				let viewDetailsViewModel = ViewDetailsViewModel(star: savedItem)
				viewDetailsViewModel.dismissPressed = { [weak self] in
					self?.userDefaults.remove(key: "selected_item")
					self?.activityIndicatorView.startAnimating()
					self?.viewModel.fetchStar()
				}
				let viewDetailsController = ViewDetailsViewController(viewModel: viewDetailsViewModel)
				let navigationController = NavigationController(rootViewController: viewDetailsController)
				navigationController.presentationController?.delegate = self
				self.navigationController?.present(navigationController, animated: true, completion: nil)
			} else {
				viewModel.fetchStar()
			}
		} catch {
			viewModel.fetchStar()
		}
    }

    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let selectedPath = tableView.indexPathForSelectedRow {
            self.tableView.deselectRow(at: selectedPath, animated: true)
        }
    }
}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.totalCount
    }

    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: TableViewCell = tableView.dequeueReusableCell(for: indexPath)
        cell.isUserInteractionEnabled = true
        cell.isAccessibilityElement = true
        cell.accessibilityIdentifier = "tableCell_\(indexPath.row)"
        let star = viewModel.results[indexPath.row]
        cell.configure(star: star)
        return cell
    }

    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let star = viewModel.results[indexPath.row]
        let viewModel = ViewDetailsViewModel(star: star)
        viewModel.dismissPressed = { [weak tableView, weak self] in
			self?.userDefaults.remove(key: "selected_item")
			self?.viewModel.sort()
            if let selectedPath = tableView?.indexPathForSelectedRow {
                tableView?.beginUpdates()
				tableView?.insertRows(at: [IndexPath(row: 0, section: 0)], with: .automatic)
				tableView?.deleteRows(at: [selectedPath], with: .automatic)
                tableView?.endUpdates()
            }
        }
        let viewController = ViewDetailsViewController(viewModel: viewModel)
        let navigationController = NavigationController(rootViewController: viewController)
        navigationController.presentationController?.delegate = self
        self.navigationController?.present(navigationController, animated: true, completion: { [weak self] in
            do {
				star.lastViewed = Date()
				try self?.userDefaults.setObject(self?.viewModel.results, forKey: "cached_stars")
                try self?.userDefaults.setObject(star, forKey: "selected_item")
            } catch {
				print(error.localizedDescription)
            }
        })
    }
}

extension ViewController: UIAdaptivePresentationControllerDelegate {
    public func presentationControllerShouldDismiss(_ presentationController: UIPresentationController) -> Bool {
        return false
    }
}
