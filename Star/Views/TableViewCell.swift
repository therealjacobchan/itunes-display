//
//  TableViewCell.swift
//  Star
//
//  Created by Jacob Chan on 11/26/20.
//  Copyright © 2020 Appetiser. All rights reserved.
//

import UIKit
import AlamofireImage

class TableViewCell: UITableViewCell, ReusableView {

    private lazy var profileImageView: UIImageView = {
        let profileImageView = UIImageView()
        profileImageView.translatesAutoresizingMaskIntoConstraints = false
        profileImageView.adjustsImageSizeForAccessibilityContentSizeCategory = true
        profileImageView.isAccessibilityElement = true
        profileImageView.clipsToBounds = true
        profileImageView.contentMode = .scaleAspectFill
        profileImageView.isAccessibilityElement = true
        profileImageView.accessibilityIdentifier = "profileImageView"
        return profileImageView
    }()

    private lazy var activityIndicatorView: UIActivityIndicatorView = {
        let avatarActivityIndicator = UIActivityIndicatorView()
        avatarActivityIndicator.color = UIColor.black
        avatarActivityIndicator.translatesAutoresizingMaskIntoConstraints = false
        avatarActivityIndicator.hidesWhenStopped = true
        return avatarActivityIndicator
    }()

    private lazy var nameLabel: UILabel = {
        let nameLabel = UILabel()
        nameLabel.translatesAutoresizingMaskIntoConstraints = false
        nameLabel.adjustsFontForContentSizeCategory = true
        nameLabel.isAccessibilityElement = true
        nameLabel.accessibilityIdentifier = "nameLabel"
        nameLabel.font = UIFont.limitedPreferredFontForTextStyle(style: .headline)
        nameLabel.numberOfLines = 0
        return nameLabel
    }()

    private lazy var genreLabel: UILabel = {
        let genreLabel = UILabel()
        genreLabel.translatesAutoresizingMaskIntoConstraints = false
        genreLabel.adjustsFontForContentSizeCategory = true
        genreLabel.isAccessibilityElement = true
        genreLabel.accessibilityIdentifier = "genreLabel"
        genreLabel.font = UIFont.limitedPreferredFontForTextStyle(style: .callout)
        genreLabel.numberOfLines = 0
        genreLabel.textColor = UIColor.lightGray
        return genreLabel
    }()

	private lazy var lastViewedLabel: UILabel = {
		let lastViewedLabel = UILabel()
		lastViewedLabel.textAlignment = .right
		lastViewedLabel.translatesAutoresizingMaskIntoConstraints = false
		lastViewedLabel.adjustsFontForContentSizeCategory = true
		lastViewedLabel.isAccessibilityElement = true
		lastViewedLabel.accessibilityIdentifier = "lastViewedLabel"
		lastViewedLabel.font = UIFont.limitedPreferredFontForTextStyle(style: .footnote)
		lastViewedLabel.textColor = UIColor.black
		return lastViewedLabel
	}()

    private lazy var middleStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.spacing = 5
        stackView.axis = .horizontal
        stackView.alignment = .fill
		stackView.distribution = .fillProportionally
		// set Content Hugging to 252
		self.genreLabel.setContentHuggingPriority(UILayoutPriority(rawValue: 252), for: .horizontal)
		self.genreLabel.setContentCompressionResistancePriority(UILayoutPriority(rawValue: 1000), for: .horizontal)
		self.lastViewedLabel.setContentHuggingPriority(UILayoutPriority(rawValue: 252), for: .horizontal)
		self.lastViewedLabel.setContentCompressionResistancePriority(UILayoutPriority(rawValue: 1000), for: .horizontal)


        stackView.addArrangedSubview(self.genreLabel)
        stackView.addArrangedSubview(self.lastViewedLabel)
        return stackView
    }()

    private lazy var priceLabel: UILabel = {
        let priceLabel = UILabel()
        priceLabel.translatesAutoresizingMaskIntoConstraints = false
        priceLabel.adjustsFontForContentSizeCategory = true
        priceLabel.isAccessibilityElement = true
        priceLabel.accessibilityIdentifier = "priceLabel"
        priceLabel.font = UIFont.limitedPreferredFontForTextStyle(style: .callout)
        priceLabel.numberOfLines = 0
        priceLabel.textColor = UIColor.lightGray
        return priceLabel
    }()

    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.distribution = .fill
        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.spacing = 2
        stackView.addArrangedSubview(self.nameLabel)
        stackView.addArrangedSubview(self.genreLabel)
        stackView.addArrangedSubview(self.priceLabel)
        return stackView
    }()

    private lazy var userDefaults: UserDefaultCredentials = {
        let userDefaults = UserDefaultCredentials()
        return userDefaults
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        configureUI()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func configureUI() {
        if profileImageView.superview == nil {
            contentView.addSubview(activityIndicatorView)
            contentView.addSubview(profileImageView)
            contentView.addSubview(nameLabel)
            contentView.addSubview(middleStackView)
            contentView.addSubview(priceLabel)
            NSLayoutConstraint.activate([
                profileImageView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10),
                profileImageView.bottomAnchor.constraint(lessThanOrEqualTo: contentView.bottomAnchor, constant: -10),
                profileImageView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 10),
                profileImageView.heightAnchor.constraint(equalToConstant: 100),
                profileImageView.widthAnchor.constraint(equalToConstant: 100),
                profileImageView.trailingAnchor.constraint(equalTo: middleStackView.leadingAnchor, constant: -10),
                activityIndicatorView.centerXAnchor.constraint(equalTo: profileImageView.centerXAnchor),
                activityIndicatorView.centerYAnchor.constraint(equalTo: profileImageView.centerYAnchor),

                nameLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10),
                nameLabel.leadingAnchor.constraint(equalTo: profileImageView.trailingAnchor, constant: 10),
                nameLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -10),
                nameLabel.bottomAnchor.constraint(equalTo: middleStackView.topAnchor, constant: -10),

                middleStackView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
                middleStackView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20),

                priceLabel.topAnchor.constraint(equalTo: middleStackView.bottomAnchor, constant: 10),
                priceLabel.leadingAnchor.constraint(equalTo: profileImageView.trailingAnchor, constant: 10),
                priceLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -10),
                priceLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -10)
            ])
        }
    }
    override func prepareForReuse() {
        super.prepareForReuse()
        profileImageView.af.cancelImageRequest()
        profileImageView.image = nil
	}

    func configure(star: Star) {
        self.nameLabel.text = star.trackName ?? ""
        self.genreLabel.text = star.primaryGenreName ?? ""
        self.priceLabel.text = "A$\(star.trackPrice ?? 0.0)"
		if let lastViewed = star.lastViewed {
			let formatter = DateFormatter()
			formatter.dateFormat = "MMM dd, yyyy"
			self.lastViewedLabel.textAlignment = .right
			self.lastViewedLabel.text = "Seen \(lastViewed.timeAgo())"
			self.lastViewedLabel.backgroundColor = .clear
			self.lastViewedLabel.textColor = .black
			self.lastViewedLabel.font = UIFont.limitedPreferredFontForTextStyle(style: .footnote)
		} else {
			self.lastViewedLabel.textAlignment = .center
			self.lastViewedLabel.text = " NEW! "
			self.lastViewedLabel.backgroundColor = UIColor.red
			self.lastViewedLabel.textColor = UIColor.white
			self.lastViewedLabel.font = UIFont.boldSystemFont(ofSize: 16)
		}
        self.activityIndicatorView.startAnimating()
        if let url = URL(string: star.artworkUrl100 ?? "") {
            self.profileImageView.af.setImage(withURL: url, placeholderImage: UIImage(named: "placeholder"), completion: { [weak self] (response) in
                guard let `self` = self else {
                    return
                }
                self.activityIndicatorView.stopAnimating()
            })
        }
    }
}
