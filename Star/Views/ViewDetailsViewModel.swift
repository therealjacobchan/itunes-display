//
//  ViewDetailsViewModel.swift
//  Star
//
//  Created by Jacob Chan on 11/26/20.
//  Copyright © 2020 Appetiser. All rights reserved.
//

import Foundation

public class ViewDetailsViewModel: NSObject {
    public let star: Star
    public let userDefaults: UserDefaultCredentials
    public var dismissPressed: (() -> ())? = nil
    init(star: Star){
        self.star = star
        self.userDefaults = UserDefaultCredentials()
    }
}
