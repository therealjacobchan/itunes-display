//
//  ViewModel.swift
//  Star
//
//  Created by Jacob Chan on 11/26/20.
//  Copyright © 2020 Appetiser. All rights reserved.
//

import UIKit

public class ViewModel: NSObject {
    var results: [Star]
    var totalCount = 0
    let starService: StarServiceProtocol
	let userDefaults: UserDefaultCredentials = UserDefaultCredentials()

    public var successfulFetch: ((Bool, String) -> Void)? = nil

    public convenience override init() {
        self.init(starService: StarService())
    }
    public init(starService: StarServiceProtocol) {
        self.results = [Star]()
        self.starService = starService
    }

	public func sort() {
		let sortedStars = self.results.sorted(by: { ($0.lastViewed ?? .distantPast) > ($1.lastViewed ?? .distantPast) })
		self.results = sortedStars
		self.totalCount = sortedStars.count
	}

    public func fetchStar() {
		do {
			if let stars = try userDefaults.getObject(forKey: "cached_stars", castTo: [Star].self) {
				let sortedStars = stars.sorted(by: { ($0.lastViewed ?? .distantPast) > ($1.lastViewed ?? .distantPast) })
				self.results = sortedStars
				self.totalCount = sortedStars.count
				self.successfulFetch?(true, "")
			} else {
				self.starService.getResults { [weak self] (response) in
					switch response {
					case .success(let star):
						if let star = star {
							do {
								try self?.userDefaults.setObject(star, forKey: "cached_stars")
							} catch {
								self?.successfulFetch?(false, error.localizedDescription)
							}
							self?.results = star
							self?.totalCount = star.count
						}
						self?.successfulFetch?(true, "")
					case .failure(let error):
						self?.successfulFetch?(false, error.localizedDescription)
					}
				}
			}
		} catch {
			self.successfulFetch?(false, error.localizedDescription)
		}
    }
}
