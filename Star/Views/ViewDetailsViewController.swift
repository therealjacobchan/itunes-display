//
//  ViewDetailsViewController.swift
//  Star
//
//  Created by Jacob Chan on 11/26/20.
//  Copyright © 2020 Appetiser. All rights reserved.
//

import UIKit

public class ViewDetailsViewController: UIViewController {

    private lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.adjustsImageSizeForAccessibilityContentSizeCategory = true
        imageView.isAccessibilityElement = true
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleAspectFill
        imageView.isAccessibilityElement = true
        imageView.accessibilityIdentifier = "imageView"
        imageView.backgroundColor = UIColor.black
        return imageView
    }()

    private lazy var activityIndicatorView: UIActivityIndicatorView = {
        let avatarActivityIndicator = UIActivityIndicatorView()
        avatarActivityIndicator.color = UIColor.black
        avatarActivityIndicator.translatesAutoresizingMaskIntoConstraints = false
        avatarActivityIndicator.hidesWhenStopped = true
        return avatarActivityIndicator
    }()

    private lazy var descriptionLabel: UILabel = {
        let descriptionLabel = UILabel()
        descriptionLabel.translatesAutoresizingMaskIntoConstraints = false
        descriptionLabel.adjustsFontForContentSizeCategory = true
        descriptionLabel.isAccessibilityElement = true
        descriptionLabel.accessibilityIdentifier = "descriptionLabel"
        descriptionLabel.font = UIFont.limitedPreferredFontForTextStyle(style: .callout)
        descriptionLabel.numberOfLines = 0
        descriptionLabel.textColor = UIColor.lightGray
        descriptionLabel.textAlignment = .center
        return descriptionLabel
    }()

    private let viewModel: ViewDetailsViewModel
    private var doneButton: UIBarButtonItem!

    public init(viewModel: ViewDetailsViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    public required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    public override func viewDidLoad() {
        super.viewDidLoad()
        extendedLayoutIncludesOpaqueBars = true
        self.title = self.viewModel.star.trackName ?? ""
        self.navigationController?.navigationBar.accessibilityIdentifier = "detailsNavigationBar"
        self.view.backgroundColor = .systemBackground
        self.view.addSubview(self.imageView)
        self.view.addSubview(self.activityIndicatorView)
        self.view.addSubview(self.descriptionLabel)
        NSLayoutConstraint.activate([
            self.imageView.topAnchor.constraint(equalTo: view.topAnchor, constant: 75),
            self.imageView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.8),
            self.imageView.heightAnchor.constraint(equalTo: self.imageView.widthAnchor, multiplier: 1.0),
            self.imageView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            self.imageView.bottomAnchor.constraint(equalTo: self.descriptionLabel.topAnchor, constant: 25),

            self.activityIndicatorView.centerXAnchor.constraint(equalTo: self.imageView.centerXAnchor),
            self.activityIndicatorView.centerYAnchor.constraint(equalTo: self.imageView.centerYAnchor),

            self.descriptionLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 10),
            self.descriptionLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -10),
            self.descriptionLabel.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 50)
        ])

        self.descriptionLabel.text = self.viewModel.star.longDescription ?? ""

        self.activityIndicatorView.startAnimating()
        if let url = URL(string: viewModel.star.artworkUrl100 ?? "") {
            self.imageView.af.setImage(withURL: url, completion: { [weak self] (response) in
                guard let `self` = self else {
                    return
                }
                self.activityIndicatorView.stopAnimating()
            })
        }

        doneButton = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.dismissVC))
        self.navigationItem.hidesBackButton = true
        self.navigationItem.rightBarButtonItem = doneButton
    }

    @objc
    func dismissVC() {
        self.viewModel.dismissPressed?()
        self.dismiss(animated: true, completion: nil)
    }
}
