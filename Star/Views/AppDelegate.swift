//
//  AppDelegate.swift
//  Star
//
//  Created by Jacob Chan on 11/26/20.
//  Copyright © 2020 Appetiser. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    var rootCoordinator: RootCoordinator?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        #if DEBUG
        if CommandLine.arguments.contains("-mockResponse") {
            stubResponses()
        }
        #endif
        window = UIWindow(frame: UIScreen.main.bounds)
        rootCoordinator = RootCoordinator()
        rootCoordinator?.start(window: window)
        return true
    }

}
