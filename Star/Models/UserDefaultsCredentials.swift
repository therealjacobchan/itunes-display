//
//  UserDefaultsCredentials.swift
//  Star
//
//  Created by Jacob Chan on 11/26/20.
//  Copyright © 2020 Appetiser. All rights reserved.
//

import Foundation

enum ObjectSavableError: String, LocalizedError {
    case unableToEncode = "Unable to encode object into data"
    case noValue = "No data object found for the given key"
    case unableToDecode = "Unable to decode object into given type"
    var errorDescription: String? {
        rawValue
    }
}

public final class UserDefaultCredentials {
    let userDefaults: UserDefaults
    public init(userDefaults: UserDefaults = .standard) {
        self.userDefaults = userDefaults
    }

    public func getBoolValue(key: String) -> Bool {
        return self.userDefaults.bool(forKey: key)
    }
    public func set(value: Bool, key: String){
        self.userDefaults.set(value, forKey: key)
    }

	public func remove(key: String) {
		self.userDefaults.removeObject(forKey: key)
	}

    public func setObject<Object>(_ object: Object, forKey: String) throws where Object: Encodable {
        let encoder = JSONEncoder()
        do {
            let data = try encoder.encode(object)
            self.userDefaults.set(data, forKey: forKey)
        } catch {
            throw ObjectSavableError.unableToEncode
        }
    }

    public func getObject<Object>(forKey: String, castTo type: Object.Type) throws -> Object? where Object: Decodable {
        guard let data = self.userDefaults.data(forKey: forKey) else { return nil }
        let decoder = JSONDecoder()
        do {
            let object = try decoder.decode(type, from: data)
            return object
        } catch {
            return nil
        }
    }
}
